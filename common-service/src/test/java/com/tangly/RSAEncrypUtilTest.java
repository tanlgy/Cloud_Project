package com.tangly;

import com.tangly.module.util.RSAEncrypUtil;
import org.junit.Test;

import static com.tangly.module.util.RSAEncrypUtil.RSADecode;
import static com.tangly.module.util.RSAEncrypUtil.RSAEncode;
import static org.junit.Assert.assertEquals;

/**
 * date: 2018/5/10 13:55 <br/>
 *
 * @author tangly
 * @since JDK 1.7
 */
public class RSAEncrypUtilTest {

    @Test
    public void testEnCodeAndDeCode() throws Exception {
        //验证加密再 解密后的值相同
        String plainText = "床前明月光疑是地上霜举头望明月低头思故乡";
        String enCodeText = RSAEncode(plainText, RSAEncrypUtil.PUBLIC_KEY);
        assertEquals(plainText, RSADecode(RSAEncrypUtil.PRIVATE_KEY, enCodeText));
    }
}