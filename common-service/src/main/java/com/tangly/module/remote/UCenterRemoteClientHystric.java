package com.tangly.module.remote;

import com.tangly.common.exception.BusinessException;
import com.tangly.common.shiro.AuthInfo;
import org.springframework.stereotype.Component;

@Component
public class UCenterRemoteClientHystric implements UCenterRemoteClient {

    @Override
    public AuthInfo getAuthInfo(String jwt) throws BusinessException {
        throw new BusinessException("授权信息验证失败");
    }
}