package com.tangly.module.remote;

import com.tangly.common.exception.BusinessException;
import com.tangly.common.shiro.AuthInfo;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 调用hi服务的客户端
 */
@FeignClient(value = "service-ucenter")
public interface UCenterRemoteClient {

    /**
     * 获取权限信息
     * @throws BusinessException
     * @param jwt
     * @return
     */
    @RequestMapping(value = "/auth/getAuthInfo",method = RequestMethod.POST)
    AuthInfo getAuthInfo(@RequestParam("jwt") String jwt) throws BusinessException;

}