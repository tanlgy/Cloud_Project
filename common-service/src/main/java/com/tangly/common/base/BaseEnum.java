package com.tangly.common.base;

import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;

/**
 * @author tangly
 * 枚举类实现该接口，以实现mybatis中的枚举键值映射为枚举类
 * 枚举类中必须包含 int型的value字段以便映射数据库
 */
public interface BaseEnum {

    String DEFAULT_VALUE_NAME = "value";

    default Integer getValue() {
        Field field = ReflectionUtils.findField(this.getClass(), DEFAULT_VALUE_NAME);
        if (field == null)
            return null;
        try {
            field.setAccessible(true);
            return Integer.parseInt(field.get(this).toString());
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}