package com.tangly.common.shiro;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 权限信息实体
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "授权信息")
public class AuthInfo  implements Serializable{

    Long userAuthId;

    Long userId;

    String username;

    String password;

    List<String> permissions;

    List<String>  roles;

}
