
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for hello_world
-- ----------------------------
DROP TABLE IF EXISTS `hello_world`;
CREATE TABLE IF NOT EXISTS `test`.`hello_world` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL COMMENT '名称',
  `phone` VARCHAR(255) NULL COMMENT '手机号',
  `type` TINYINT NULL DEFAULT 0 COMMENT 'helloworld类型，测试枚举类',
  `sex` TINYINT NULL DEFAULT 0 COMMENT '性别枚举 0女 1男 2未知',
  `email` VARCHAR(45) NULL,
  `address` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
  ENGINE = InnoDB
  AUTO_INCREMENT = 15
  DEFAULT CHARACTER SET = utf8mb4
  COMMENT = '示例对象表格'
