package com.tangly.module.mapper;

import com.tangly.common.base.BaseMybatisMapper;
import com.tangly.module.entity.HelloWorld;
import org.springframework.stereotype.Repository;

/**
 * 用注解形式的查询
 * @author tangly
 */
@Repository
public interface HelloWorldMapper extends BaseMybatisMapper<HelloWorld> {

}