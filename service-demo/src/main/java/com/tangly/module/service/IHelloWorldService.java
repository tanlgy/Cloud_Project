package com.tangly.module.service;


import com.tangly.common.base.IBaseService;
import com.tangly.module.entity.HelloWorld;

/**
 * date: 2018/1/2 17:37 <br/>
 *
 * @author tangly
 * @since JDK 1.7
 */
public interface IHelloWorldService extends IBaseService<HelloWorld> {


}
