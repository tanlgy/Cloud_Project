package com.tangly.module.service.impl;

import com.tangly.common.base.BaseServiceImpl;
import com.tangly.module.entity.HelloWorld;
import com.tangly.module.service.IHelloWorldService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author tangly
 * @since JDK 1.7
 */
@Service
@Slf4j
public class HelloWorldServiceImplImpl extends BaseServiceImpl<HelloWorld> implements IHelloWorldService {

}
