INSERT INTO test.user_info (user_nickname, user_avatar, user_sex) VALUES ('张三', '上传的头像地址', null);

INSERT INTO test.user_auth (user_info_id, auth_available, login_type, login_account, login_password, login_salt, last_login_ip, last_login_time, last_login_device, last_login_try_ip, last_login_try_count, last_login_token, create_time, update_time) VALUES (1, true, '0', 'supervisor', 'abc4f152dae9c026ad88686a381a059c', 'a2bd8b0918bad06cb925a0b14b02cc29', '192.168.1.10', '2018-05-27 23:07:56', null, '0.0.0.0', 0, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1Mjc1MjAwNzYsInVzZXJJZCI6MSwiYXV0aElkIjoxfQ.5nuR4od86nPdA9rGT2zaX2_V30gjfwUxB6UuWsScNXU', '2018-05-27 23:05:48', null);

INSERT INTO test.sys_role (available, description, name, level) VALUES (true, '超级管理员', 'SUPERVISOR', 0);

INSERT INTO test.user_info_has_sys_role (user_info_id, sys_role_id) VALUES (1, 1);

INSERT INTO test.sys_permission (name, available, description) VALUES ('*:*', true, '所有模块');

INSERT INTO test.sys_permission_has_sys_role (sys_permission_id, sys_role_id) VALUES (1, 1);
