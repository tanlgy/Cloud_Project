package com.tangly.module.service;

import com.tangly.ServiceUCenterApplication;
import com.tangly.module.entity.UserAuth;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ServiceUCenterApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class IUserAuthServiceTest {

    @Autowired
    private IUserAuthService iUserAuthService;

    @Test
    public void getUserAuthList() throws Exception {
        List<UserAuth> list = iUserAuthService.getUserAuthList();
        System.out.println(list);
    }

}