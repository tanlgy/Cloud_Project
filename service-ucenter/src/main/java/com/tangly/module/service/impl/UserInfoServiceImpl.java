package com.tangly.module.service.impl;

import com.tangly.common.base.BaseServiceImpl;
import com.tangly.module.entity.UserInfo;
import com.tangly.module.service.IUserInfoService;
import org.springframework.stereotype.Service;

/**
 * @author tangly
 * @since JDK 1.7
 */
@Service
public class UserInfoServiceImpl extends BaseServiceImpl<UserInfo> implements IUserInfoService {
}
