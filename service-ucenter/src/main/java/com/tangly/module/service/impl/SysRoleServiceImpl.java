package com.tangly.module.service.impl;

import com.tangly.common.base.BaseServiceImpl;
import com.tangly.module.entity.SysRole;
import com.tangly.module.mapper.SysRoleMapper;
import com.tangly.module.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * date: 2018/5/17 16:15 <br/>
 *
 * @author Administrator
 * @since JDK 1.7
 */
@Service
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole> implements ISysRoleService{

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public List<SysRole> getSysRole(Long userId) {
        return sysRoleMapper.getSysRole(userId);
    }
}
