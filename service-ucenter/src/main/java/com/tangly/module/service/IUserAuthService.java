package com.tangly.module.service;


import com.tangly.common.base.IBaseService;
import com.tangly.module.entity.UserAuth;

import java.util.List;

/**
 * date: 2018/5/2 10:23 <br/>
 * 用户账号接口类
 * @author tangly
 * @since JDK 1.7
 */
public interface IUserAuthService extends IBaseService<UserAuth> {

    /**
     * 根据用户登录账号取账号登录实体
     * @param loginAccount
     * @return
     */
    UserAuth getUserAuth(String loginAccount);

    /**
     * 根据用户登录ID取账号登录实体
     * @param userAuthId
     * @return
     */
    UserAuth getUserAuthById(Long userAuthId);

    /**
     * 注册用户
     * @param userAuth
     */
    int registerUserAuth(UserAuth userAuth);

    /**
     * 检验用户名是否存在
     * @param loginAccount
     * @return
     */
    boolean existUserName(String loginAccount);

    /**
     * 保存用户
     * @param userAuth
     */
    void save(UserAuth userAuth);

    /**
     * 记录最后一次登录失败
     * @param userAuth
     * @param requestIP
     */
    void updateLoginInfoFail(UserAuth userAuth, String requestIP);

    /**
     * 更新最新一次登录成功
     * @param userAuth
     * @param requestIP
     * @param token
     */
    void updateLoginInfoSuccess(UserAuth userAuth, String requestIP, String token);

    /**
     * 获取账号列表
     * @return
     */
    List<UserAuth> getUserAuthList();
}
