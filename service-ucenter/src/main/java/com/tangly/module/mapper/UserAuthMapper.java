package com.tangly.module.mapper;

import com.tangly.common.base.BaseMybatisMapper;
import com.tangly.module.entity.UserAuth;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.mapping.FetchType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAuthMapper extends BaseMybatisMapper<UserAuth> {

    /**
     * 根据用户登录账号获取用户信息
     * @param loginAccount
     * @return
     */
    UserAuth getUserAuthByAccount(String loginAccount);

    /**
     * 根据用户登录ID获取用户信息
     * @param authId
     * @return
     */
    UserAuth getUserAuthById(Long authId);

    /**
     * 获取用户账号列表
     * @return
     */
    List<UserAuth> getUserAuthList();

}