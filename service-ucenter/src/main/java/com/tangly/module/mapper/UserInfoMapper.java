package com.tangly.module.mapper;

import com.tangly.common.base.BaseMybatisMapper;
import com.tangly.module.entity.UserInfo;
import org.springframework.stereotype.Repository;

@Repository
public interface UserInfoMapper extends BaseMybatisMapper<UserInfo> {
}