package com.tangly;

import com.tangly.common.base.BaseMybatisMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
@MapperScan(basePackages = "com.tangly.module.mapper", markerInterface = BaseMybatisMapper.class)
public class ServiceUCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceUCenterApplication.class, args);
	}
}
