package com.tangly.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigController {

    @Value("${test}")
    String test;

    @RequestMapping(value = "/test")
    public String test(){
        return test;
    }
}
