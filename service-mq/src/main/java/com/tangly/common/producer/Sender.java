package com.tangly.common.producer;

import com.tangly.common.config.ConnectionConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Sender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void send(String string) {
        rabbitTemplate.convertAndSend(ConnectionConfig.QUEUE, string);
    }
}
