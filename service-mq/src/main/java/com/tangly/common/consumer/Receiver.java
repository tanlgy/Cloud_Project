package com.tangly.common.consumer;

import com.tangly.common.config.ConnectionConfig;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = ConnectionConfig.QUEUE)
public class Receiver {

    @RabbitHandler
    public void process(String message) {
        System.out.println("收到消息: '" + message + "'");
    }

}