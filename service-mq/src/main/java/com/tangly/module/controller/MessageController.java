package com.tangly.module.controller;

import com.tangly.common.producer.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {

    @Autowired
    private Sender runner;

    @GetMapping("/publish")
    public String publish(@RequestParam("content") String content) {
        runner.send(content);
        return content;
    }
}
